<?php

use Faker\Generator as Faker;
use Proexe\BookingApp\Offices\Models\OfficeModel;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define( OfficeModel::class, function ( Faker $faker ) {
	return [
		'name'         => $faker->colorName,
		'lat'          => $faker->latitude,
		'lng'          => $faker->longitude,
		'office_hours' => [
			0 => [ $faker->numberBetween( 8, 12 ) . ':00', $faker->numberBetween( 15, 20 ) . ':00' ],
			1 => [ $faker->numberBetween( 8, 12 ) . ':00', $faker->numberBetween( 15, 20 ) . ':00' ],
			2 => [ $faker->numberBetween( 8, 12 ) . ':00', $faker->numberBetween( 15, 20 ) . ':00' ],
			3 => [ $faker->numberBetween( 8, 12 ) . ':00', $faker->numberBetween( 15, 20 ) . ':00' ],
			4 => [ $faker->numberBetween( 8, 12 ) . ':00', $faker->numberBetween( 15, 20 ) . ':00' ],
			5 => [ $faker->numberBetween( 8, 12 ) . ':00', $faker->numberBetween( 15, 20 ) . ':00' ],
			6 => [ $faker->numberBetween( 8, 12 ) . ':00', $faker->numberBetween( 15, 20 ) . ':00' ],
		],
	];
} );